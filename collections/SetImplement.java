package collections;

import java.util.HashSet;
import java.util.Set;

public class SetImplement {
    public static void main(String[] args) {
        //instance object
        Set<String> months = new HashSet<>();
        Set<Integer> numbers = new HashSet<>();

        //add data
        months.add("Januari");
        months.add("Februari");
        months.add("Maret");
        months.add("April");
        months.add("Mei");
        months.add("Juni");
        System.out.println(months);

        //update data
        months.remove("Januari");
        months.add("Juli");
        System.out.println(months);

        //hapus data
        months.remove("Maret");
        System.out.println(months);

        System.out.println(numbers);
    }   
}
