import bangunDatar.Persegi;
import bangunDatar.Segitiga;

public class App {
    public static void main(String[] args) {
        System.out.println("Belajar JAVA");

        Persegi persegi = new Persegi();
        persegi.sisiPersegi = 10;
        persegi.cetakSisi();
        System.out.println(persegi.getSisi());
        persegi.cetakKeliling();
        persegi.cetakLuas();

        // instance object segitiga
        Segitiga segitiga = new Segitiga();
        segitiga.setAlas(10);
        segitiga.setTinggi(5);
        segitiga.setSisiMiring(7);
        segitiga.cetakSegitiga(5);

        System.out.println(segitiga.getAlas());
        System.out.println(segitiga.getTinggi());
        System.out.println(segitiga.getSisiMiring());

        System.out.println("keliling: " + segitiga.getKeliling());
        System.out.println("luas: " + segitiga.getLuas());

    }
}