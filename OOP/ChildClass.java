package OOP;

/*inheritance
 * suatu class dpt diwarisi di class lainnya
 * keyword 
 * 1. extends: Class - Class atau Interface ke Interface 
 * 2. implement: Interface - Class
 */
public class ChildClass extends ParentClass {
    private String kalimat;

    
    public ChildClass(String kalimat) {
        this.kalimat = kalimat;
    }

    /*1. super = representasi */
    public ChildClass(String tulisan, int number, String kalimat) {
        super(tulisan, number);
        super.setTeks(kalimat);
        this.kalimat = kalimat;
        this.setKalimat(tulisan);
    }

    public ChildClass() {
    }

    public String getKalimat() {
        return kalimat;
    }

    public void setKalimat(String kalimat) {
        this.kalimat = kalimat;
    }

    //override method
    @Override
    public void greeting(String sapa) {
        System.out.println(sapa + " ini berasal dari child class");
    }
    
}
