package collections;

import java.util.ArrayList;
import java.util.List;

public class ListImplement {
    public static void main(String[] args) {
        List<String> toDo = new ArrayList<>();

        // Add data
        toDo.add("Belanja");
        toDo.add("Ngoding");
        toDo.add("Makan");
        toDo.add("Makan");

        //get data
        System.out.println(toDo.get(2));

        //update 
        toDo.set(3, "Minum");

        //remove data
        toDo.remove(0);

        System.out.println(toDo);
      
    }
    
}
