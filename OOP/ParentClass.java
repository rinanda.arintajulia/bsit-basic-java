package OOP;

public class ParentClass {
    private String teks;
    private int angka;

    public ParentClass() {

    }

    public ParentClass(String tulisan, int number) {
        this.angka = number;
        this.teks = tulisan;
    }

    public ParentClass(String tulisan, int number, String contoj) {
        this.angka = number;
        this.teks = tulisan;
    }

    public String getTeks() {
        return teks;
    }

    
    public String getTeks(String text) {
        System.out.println("ini text dari method getTeks" + text);
        return teks;
    }

    public void setTeks(String teks) {
        this.teks = teks;
    }

    public int getAngka() {
        return angka;
    }

    public void setAngka(int angka) {
        this.angka = angka;
    }

    public void greeting(String sapa) {
        System.out.println(sapa + "ini berasal dari parent class");
    }
}