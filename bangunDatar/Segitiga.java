package bangunDatar;

public class Segitiga {
    /*Modifier
     * 
     1. public: semua atribut, class, method, bisa dipanggil
     2. default: hanya bisa dipanggil dalam class dan package yg sama
     3. private: hanya bisa diakses di class yg sama
     4. protected: bisa dipanggil 
     */

    //Atribut segitiga
    private int alas, tinggi, sisiMiring;

    //Method Setter - Getter
    /*
     * Setter=untuk set value atribute
     * Getter=untuk get value atribute
     */

     //Method setter
     public void setAlas(int alasSegitiga) {
        alas = alasSegitiga;
     }

     public void setTinggi(int tinggi) {
        this.tinggi = tinggi;
}
    public void setSisiMiring(int sisiMiring) {
        this.sisiMiring = sisiMiring;
}
    //method getter
    public int getAlas() {
        return alas;
    }

    public int getTinggi() {
        return tinggi;
    }

    public int getSisiMiring() {
        return sisiMiring;
    }

    //method custom
    public int getKeliling() {
        int keliling = alas + tinggi + sisiMiring;
        return keliling;
    }

    public float getLuas() {
        float luas = alas * tinggi / 2;
        return luas;
    }

    public void cetakSegitiga(int sisi) {
        for (int i = 0; i < sisi; i++) {
            for (int j = 0; j < sisi; j++) {
                if (i >= j) {
                    System.out.print("*");
                }
            }System.out.println();
        }

    }
    }